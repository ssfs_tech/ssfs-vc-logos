# Veracross Logo Hosting for SSFS

- [`README.md`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//README.md)
- `img`
    - `logos`
        - [`square_125x125.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/logos/square_125x125.png)
        - [`rectangle_green_bg.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/logos/rectangle_green_bg.png)
        - [`rectangle.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/logos/rectangle.png)
        - [`square_300x300.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/logos/square_300x300.png)
        - [`rectangle_white_bg.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/logos/rectangle_white_bg.png)
        - [`square_100x100.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/logos/square_100x100.png)
        - [`circle.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/logos/circle.png)
    - `campus`
        - [`vc_axiom_banner_01.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/campus/vc_axiom_banner_01.png)
        - [`vc_login_campus_01.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/campus/vc_login_campus_01.png)
        - [`vc_axiom_banner_orig.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/campus/vc_axiom_banner_orig.png)
        - [`vc_login_campus_02.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/campus/vc_login_campus_02.png)
        - [`vc_login_campus_03.png`](https://bitbucket.org/ssfs_tech/ssfs-vc-logos/raw/master//img/campus/vc_login_campus_03.png)
